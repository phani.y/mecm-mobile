/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../pages/Layout.vue'
Vue.use(Router)

export default new Router({
  routes: [
    /** *********Routing For normal UI**************/
    {
      path: '/',
      name: 'mec-overview',
      component: () => import('../overview/Overview.vue')
    }, {
      path: '/index',
      redirect: '/',
      component: () => import('../overview/Overview.vue')
    },
    {
      path: '',
      name: 'layout',
      component: Layout,
      children: [
        {
          path: 'mecm/apac/list',
          name: 'apaclist',
          component: () => import('../app/PackageList.vue')
        },
        {
          path: 'mecm/ruleconfig',
          name: 'rule',
          component: () => import('../app/RuleConfig.vue')
        },
        {
          path: 'mecm/edge/list',
          name: 'edgelist',
          component: () => import('../app/EdgeList.vue')
        },
        {
          path: 'mecm/apac/detail',
          name: 'apacdetail',
          component: () => import('../app/PackageDetail.vue')
        },
        {
          path: 'mecm/ains/list',
          name: 'ainslist',
          component: () => import('../app/InstanceList.vue')
        },
        {
          path: 'mecm/node/list',
          name: 'hostOverview',
          component: () => import('../host/List.vue')
        },
        {
          path: 'mecm/systems/external/applcm',
          name: 'externalSystem1',
          component: () => import('../system/Applcm.vue')
        },
        {
          path: 'mecm/systems/external/apprule',
          name: 'externalSystem4',
          component: () => import('../system/Apprule.vue')
        },
        {
          path: 'mecm/systems/external/edgenodes',
          name: 'externalSystem2',
          component: () => import('../system/Ks.vue')
        },
        {
          path: 'mecm/systems/external/store',
          name: 'externalSystem3',
          component: () => import('../system/Appstore.vue')
        }
      ]
    },

    /** *****************Mobile Routing***********/
    {
      path: '/mobile/overview',
      name: 'mec-overview',
      component: () => import('../mobile-layout/overview/Overview.vue')
    }, {
      path: '/index',
      redirect: '/',
      component: () => import('../mobile-layout/overview/Overview.vue')
    },
    {
      path: '',
      name: 'layout',
      component: Layout,
      children: [
        {
          path: 'mobile/mecm/apac/list',
          name: 'apaclist',
          component: () => import('../mobile-layout/app/PackageList.vue')
        },
        {
          path: 'mobile/mecm/ruleconfig',
          name: 'rule',
          component: () => import('../mobile-layout/app/RuleConfig.vue')
        },
        {
          path: 'mobile/mecm/edge/list',
          name: 'edgelist',
          component: () => import('../mobile-layout/app/EdgeList.vue')
        },
        {
          path: 'mobile/mecm/apac/detail',
          name: 'apacdetail',
          component: () => import('../mobile-layout/app/PackageDetail.vue')
        },
        {
          path: 'mobile/mecm/ains/list',
          name: 'ainslist',
          component: () => import('../mobile-layout/app/InstanceList.vue')
        },
        {
          path: 'mobile/mecm/node/list',
          name: 'hostOverview',
          component: () => import('../mobile-layout/host/List.vue')
        },
        {
          path: 'mobile/mecm/systems/external/applcm',
          name: 'externalSystem1',
          component: () => import('../mobile-layout/system/Applcm.vue')
        },
        {
          path: 'mobile/mecm/systems/external/apprule',
          name: 'externalSystem4',
          component: () => import('../mobile-layout/system/Apprule.vue')
        },
        {
          path: 'mobile/mecm/systems/external/edgenodes',
          name: 'externalSystem2',
          component: () => import('../mobile-layout/system/Ks.vue')
        },
        {
          path: 'mobile/mecm/systems/external/store',
          name: 'externalSystem3',
          component: () => import('../mobile-layout/system/Appstore.vue')
        }
      ]
    }

  ]
})
