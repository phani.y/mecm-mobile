/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const NavData = [
  {
    'id': '2.1',
    'icon': '',
    'name': 'Overview',
    'path': '/mobile/overview',
    'pageId': '2.0',
    'display': false
  },
  {
    'id': '2.2',
    'name': 'APP Management',
    'path': '/mobile/mecm/app/pakage',
    'pageId': '2.0.1',
    'display': false,
    'children': [
      {
        'id': '2.3',
        'name': 'Package Management',
        'path': '/mobile/mecm/apac/list',
        'pageId': '2.0.1.1',
        'display': false
      },
      {
        'id': '2.5',
        'name': 'App Instance List',
        'path': '/mobile/mecm/ains/list',
        'pageId': '2.0.1.3',
        'display': false
      }
    ]
  },
  {
    'id': '2.6',
    'name': 'Edge Nodes',
    'path': '/mobile/mecm/node/list',
    'pageId': '2.0.2',
    'display': false
  },
  {
    'id': '2.9',
    'name': 'Systems',
    'path': '/mobile/mecm/systems',
    'pageId': '2.0.3',
    'display': false,
    'children': [
      {
        'id': '2.11',
        'name': 'App LCM',
        'path': '/mobile/mecm/systems/external/applcm',
        'pageId': '2.0.3.1.1',
        'display': false
      },
      {
        'id': '2.14',
        'name': 'App Rule',
        'path': '/mobile/mecm/systems/external/apprule',
        'pageId': '2.0.3.1.4',
        'display': false
      },
      {
        'id': '2.12',
        'name': 'Edge Node',
        'path': '/mobile/mecm/systems/external/edgenodes',
        'pageId': '2.0.3.1.2',
        'display': false
      },
      {
        'id': '2.13',
        'name': 'App Store',
        'path': '/mobile/mecm/systems/external/store',
        'pageId': '2.0.3.1.3',
        'display': false
      }
    ]
  }
]

export default NavData
